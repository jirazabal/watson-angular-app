require('dotenv').load({ silent: true });
const express = require('express');
const cors = require('cors');
const app = express();
const path = require('path');
const secure = require('express-secure-only');
const RateLimit = require('express-rate-limit');
const { assistantRoutes, ttsRoutes, sttRoutes } = require('./routes');
const limiter = new RateLimit({
  windowMs: 15 * 60 * 1000, // 15 minutes
  max: 100, // limit each IP to 100 requests per windowMs
});

// allows environment properties to be set in a file named .env
app.use(cors({ origin: '*' }));
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept'
  );
  next();
});
/*************************
// app.enable('trust proxy'); // required to work properly behind Bluemix's reverse proxy
// app.use(secure());
**************************/
//  apply to /api/*
app.use('/api/', limiter);

app.use(express.json({ limit: '20mb' }));
app.use(express.urlencoded({ extended: false, limit: '20mb' }));
app.use(
  '/',
  express.static(
    path.join(__dirname + '\\..\\client\\dist\\watson-angular-app')
  )
);

app.use('/api/assistant', assistantRoutes);
app.use('/api/stt', sttRoutes);
app.use('/api/tts', ttsRoutes);

module.exports = app;
