import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Message } from '@app/models';
import { NzModalService } from 'ng-zorro-antd/modal';

@Component({
  selector: 'app-message-item',
  templateUrl: './message-item.component.html',
  styleUrls: ['./message-item.component.scss'],
})
export class MessageItemComponent implements OnInit {
  @Input()
  message: Message;
  @Input()
  messages: Message[];

  @Output()
  labelClicked = new EventEmitter<string>();

  messageUrl: string;

  constructor(private modalService: NzModalService) {}

  ngOnInit() {}

  onLabelClicked(text: string) {
    this.labelClicked.emit(text);
  }

  /**
   *
   * @returns string
   * Parses incoming text stream and detects a hyperlink in the results so it can render properly
   */
  displayMessage(): string {
    let hyperlinkFound = false;
    const textArr = this.message.text.split(' ');
    textArr.forEach((text, index) => {
      const regEx = /http/;
      if (text.search(regEx) !== -1) {
        hyperlinkFound = true;
        this.messageUrl = text;
        textArr.splice(index, 1);
      }
    });
    if (hyperlinkFound) {
      this.message.text = textArr.join(' ');
    }
    return this.message.text;
  }
}
