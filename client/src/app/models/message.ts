import { Result } from './response';

export class Message {
  text: string;
  sentBy: string;
  emotion: string;
  intent: string;
  url: string;
  title: string;
  description: string;
  results: Result[];

  constructor(
    text?: string,
    sentBy?: string,
    emotion?: string,
    intent?: string,
    url?: string,
    title?: string,
    description?: string,
    results?: Result[]
  ) {
    this.text = text;
    this.sentBy = sentBy;
    this.emotion = emotion;
    this.intent = intent;
    this.url = url;
    this.title = title;
    this.description = description;
    this.results = results;
  }
}
