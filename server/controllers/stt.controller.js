const { IamTokenManager } = require('ibm-watson/auth');
const logger = require('../config/logger');

class STTController {
  getToken(req, res) {
    const sttAuthenticator = new IamTokenManager({
      apikey: process.env.SPEECH_TO_TEXT_IAM_APIKEY,
    });
    return sttAuthenticator
      .requestToken()
      .then(({ result }) => {
        logger.info('Watson Speech to Text: Auth request success');
        res.json({
          accessToken: result.access_token,
          url: process.env.SPEECH_TO_TEXT_URL,
        });
      })
      .catch(console.error);
  }
}

module.exports = STTController;
