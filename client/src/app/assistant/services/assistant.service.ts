import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AssistantResponse, Context } from '@app/models';
import { SessionIdResponse } from '@app/models/response';
import { environment } from '@env/environment';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};

@Injectable({
  providedIn: 'root',
})
export class AssistantService {
  host = environment.url;
  path = '/api/assistant';

  url = this.host + this.path;

  constructor(private http: HttpClient) {}

  getSessionId(): Observable<SessionIdResponse> {
    return this.http.get<SessionIdResponse>(`${this.url}/session`, httpOptions);
  }

  message(
    text: string,
    context: Context,
    sessionId: string
  ): Observable<AssistantResponse> {
    return this.http.post<AssistantResponse>(
      `${this.url}/message`,
      { message: text, context: context, sessionId },
      httpOptions
    );
  }
}
