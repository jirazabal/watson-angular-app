import { Component, EventEmitter, Inject, Input, NgZone, OnInit, Output } from '@angular/core';
import { AssistantService, SttService, TtsService } from '@app/assistant/services';
import { Context, Generic, Message } from '@app/models';
import * as recognizeMicrophone from 'watson-speech/speech-to-text/recognize-microphone';
import { synthesize } from 'watson-speech/text-to-speech';

@Component({
  selector: 'app-message-form',
  templateUrl: './message-form.component.html',
  styleUrls: ['./message-form.component.scss'],
})
export class MessageFormComponent implements OnInit {
  @Input()
  labelClicked;
  @Input()
  messages: Message[];
  @Input()
  message: Message;
  context: Context;
  @Input()
  firstDisplayed;
  @Output()
  firstDisplayedChange = new EventEmitter<boolean>();

  watsonAudio: HTMLAudioElement;
  micActive = false;
  isStreaming: boolean;
  stream: any;
  text: string;
  SSTtoken: { accessToken: string; url: string };
  TTStoken: { accessToken: string; url: string };
  private sessionId;
  constructor(
    @Inject(AssistantService) private assistantService: AssistantService,
    @Inject(TtsService) private ttsService: TtsService,
    @Inject(SttService) private sttService: SttService,
    @Inject(NgZone) private ngZone: NgZone
  ) {}

  ngOnInit() {
    this.getSttToken();
    this.getTtsToken();
    this.message = new Message();
    if (this.firstDisplayed) {
      // grab session id
      this.assistantService.getSessionId().subscribe((sessionIdResponse) => {
        this.sessionId = sessionIdResponse.session_id;
        this.assistantService.message('', null, this.sessionId).subscribe(async (data) => {
          this.firstDisplayedChange.emit(false);
          this.displayDots('watson');
          await this.displayMessage(data.output.generic[0].text);
        });
      });
    }

    this.labelClicked.subscribe((text) => {
      this.onLabelClicked(text);
    });
  }
  /**
   * Fetches IBM STT Auth Token
   */
  getSttToken(): void {
    this.sttService.fetchSTTToken().subscribe((token) => (this.SSTtoken = token));
  }
  /**
   * Fetches IBM TTS Auth Token
   */
  getTtsToken(): void {
    this.ttsService.fetchTTSToken().subscribe((token) => (this.TTStoken = token));
  }

  /**
   * Starts mic audio stream and process speech input
   */
  startStream(): void {
    this.isStreaming = true;
    this.micActive = true;
    this.stream = recognizeMicrophone(
      Object.assign(this.SSTtoken, {
        format: true,
        extractResults: true,
        objectMode: true,
      })
    );
    // Use ngZone to enable events to update outside Angular Lifecycle
    this.ngZone.runOutsideAngular(() => {
      this.stream.on('data', (data) => {
        this.ngZone.run(() => {
          this.text = data.alternatives[0].transcript;
          if (data.alternatives[0] && data.final) {
            this.stream.stop();
            this.micActive = false;
            this.isStreaming = false;
            this.message.text = this.text;
            this.sendMessage();
          }
        });
      });
      this.stream.on('error', function (err) {
        console.error(err);
      });
    });
  }
  /**
   * Stops audio stream
   */
  stopStream(): void {
    if (this.stream) {
      this.playAudio('../../assets/sounds/mic_close.wav');
      this.micActive = false;
      this.isStreaming = false;
      this.stream.stop();
    }
  }
  /**
   * User click on mic button
   */
  micOnClick() {
    this.playAudio('../../assets/sounds/mic_open.wav');
    this.startStream();
  }
  /**
   * Plays mic open/close beep
   */
  playAudio(pathToAudioFile: string) {
    // If Watson is currently speaking, stop the playback
    if (this.watsonAudio) {
      this.watsonAudio.pause();
      this.watsonAudio.currentTime = 0;
    }
    let audio = new Audio();
    audio.src = pathToAudioFile;
    audio.load();
    audio.play();
  }

  sendMessage() {
    // send the user message
    this.message.sentBy = 'user';
    this.messages.push(this.message);
    this.scroll();

    // Get Watson's response from the Assistant Service
    this.assistantService.message(this.message.text, this.context, this.sessionId).subscribe(async (data) => {
      this.context = data.context;

      // If the message is sent by Discovery
      if (data.output.generic[0].response_type === 'search') {
        if (!data.output.generic[0].results.length) {
          this.displayDots('watson');
          await this.displayMessage(data.output.generic[0]?.header);
        } else {
          this.displayDots('discovery');
          await this.displayDiscoveryMessage(data.output.generic[0]);
        }
      } else {
        if (data.output.generic[0].response_type === 'option') {
          this.displayDots('option');
          await this.displayOptions(data.output.generic[0]);
        } else {
          this.displayDots('watson');
          if (data.output.generic[0].header) {
            await this.displayMessage(data.output.generic[0]?.header);
          } else {
            await this.displayMessage(data.output.generic[0]?.text);
          }
          if (data.output.generic[0]?.results?.length) {
            await this.displayMessage(data.output.generic[0].results[0].body);
          }
        }
      }
    });

    // Create a new message to clear the input
    this.message = new Message();
  }
  async playWatsonResponse(textstr: string) {
    this.watsonAudio = await synthesize(
      Object.assign(this.TTStoken, {
        text: textstr,
        voice: 'en-US_AllisonVoice',
      })
    );
    this.watsonAudio.onerror = function (err) {
      console.error('audio error: ', err);
    };
  }

  displayOptions(generic: Generic): Promise<void> {
    return new Promise((resolve) => {
      setTimeout(
        () => {
          this.messages[this.messages.length - 1].text = generic.header;
          this.messages[this.messages.length - 1].description = generic.description;
          this.messages[this.messages.length - 1].results = generic.results;
          this.scroll();
          resolve();
        },
        generic.title.length * 60 > 1500 ? 1500 : generic.title.length * 60
      );
    });
  }

  displayDots(sentBy: string): void {
    this.messages.push(new Message('...', sentBy));
    this.scroll();
  }

  displayMessage(textstr: string): Promise<void> {
    return new Promise((resolve) => {
      setTimeout(
        () => {
          // Play TTS response from Watson
          this.playWatsonResponse(textstr);
          // Push Watson response as text to be rendered on screen
          this.messages[this.messages.length - 1].text = textstr;
          this.scroll();
          resolve();
        },
        textstr.length * 30 > 1500 ? 1500 : textstr.length * 30
      );
    });
  }

  displayDiscoveryMessage(generic: Generic): Promise<void> {
    return new Promise((resolve) => {
      setTimeout(
        () => {
          // Play TTS response from Watson
          this.playWatsonResponse(generic.results[0].body || generic.header);

          this.messages[this.messages.length - 1].text = generic.header;
          this.messages[this.messages.length - 1].results = generic.results.slice(0, 3);
          this.scroll();
          resolve();
        },
        generic.header.length * 60 > 1500 ? 1500 : generic.header.length * 60
      );
    });
  }

  onLabelClicked(text: string) {
    this.message.text = text;
    this.sendMessage();
  }

  scroll() {
    const messages = document.getElementById('scroll');
    setTimeout(() => {
      messages?.scrollTo({
        top: messages?.scrollHeight,
        behavior: 'smooth',
      });
    }, 100);
  }
}
export interface RecognizeStream {
  token: string;
  format?: boolean;
  keepMicrophone?: boolean;
  outputElement?: string;
  extractResults?: boolean;
  objectMode?: boolean;
  resultsBySpeaker?: boolean;
  mediaStream?: MediaStream;
}
