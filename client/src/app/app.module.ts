import { DragDropModule } from '@angular/cdk/drag-drop';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { registerLocaleData } from '@angular/common';
import { HttpClientJsonpModule, HttpClientModule } from '@angular/common/http';
import en from '@angular/common/locales/en';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { IconDefinition } from '@ant-design/icons-angular';
import * as AllIcons from '@ant-design/icons-angular/icons';
import { en_US, NZ_I18N } from 'ng-zorro-antd/i18n';
import { NZ_ICONS } from 'ng-zorro-antd/icon';
import { AppComponent } from './app.component';
import { AssistantComponent } from './assistant/assistant.component';
import { AssistantHeaderComponent } from './assistant/header/header.component';
import { MessageFormComponent } from './assistant/message-form/message-form.component';
import { MessageItemComponent } from './assistant/message-item/message-item.component';
import { MessageListComponent } from './assistant/message-list/message-list.component';
import { ContentComponent } from './layout/content/content.component';
import { FooterComponent } from './layout/footer/footer.component';
import { HeaderComponent } from './layout/header/header.component';
import { LayoutComponent } from './layout/layout.component';
import { SiderComponent } from './layout/sider/sider.component';
import { NgZorroAntdModule } from './ng-zorro-antd.module';
import { ReadMoreComponent } from './layout/read-more/read-more.component';

registerLocaleData(en);

const antDesignIcons = AllIcons as {
  [key: string]: IconDefinition;
};
const icons: IconDefinition[] = Object.keys(antDesignIcons).map(
  (key) => antDesignIcons[key]
);

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    HttpClientJsonpModule,
    ReactiveFormsModule,
    NgZorroAntdModule,
    NoopAnimationsModule,
    ScrollingModule,
    DragDropModule,
  ],
  declarations: [
    AppComponent,
    AssistantComponent,
    MessageListComponent,
    MessageItemComponent,
    MessageFormComponent,
    HeaderComponent,
    AssistantHeaderComponent,
    SiderComponent,
    FooterComponent,
    LayoutComponent,
    ContentComponent,
    ReadMoreComponent,
  ],
  bootstrap: [AppComponent],
  providers: [
    { provide: NZ_I18N, useValue: en_US },
    { provide: NZ_ICONS, useValue: icons },
  ],
})
export class AppModule {}
