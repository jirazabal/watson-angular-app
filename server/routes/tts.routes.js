const router = require('express').Router();
const { TTSController } = require('../controllers');
const logger = require('../config/logger');

const ttsController = new TTSController();

router.post('/getToken', (req, res) => {
  logger.info('[Text to Speech - /getToken - POST]');
  ttsController.getToken(req, res);
});

module.exports = router;
