const server = require('./app');
const port = process.env.PORT;
const fs = require('fs');
const https = require('https');
const logger = require('./config/logger');

const options = {
  key: fs.readFileSync(__dirname + '/keys/localhost.pem'),
  cert: fs.readFileSync(__dirname + '/keys/localhost.cert'),
};
// https.createServer(options, server).listen(port, function () {
//   logger.info(`Secure server live at https://localhost:${port}/`);
// });

server.listen(port, function () {
  logger.info(`Server live at http://localhost:${port}/`);
});
