const router = require('express').Router();
const { AssistantController } = require('../controllers');
const assistantController = new AssistantController();

router.post('/message', (req, res) => {
  assistantController.postMessage(req.body, res);
});
router.get('/session', (req, res) => {
  assistantController.getSessionId(req.body, res);
});

module.exports = router;
