module.exports = {
  assistantRoutes: require('./assistant.routes'),
  sttRoutes: require('./stt.routes'),
  ttsRoutes: require('./tts.routes'),
};
