import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss'],
})
export class ContentComponent implements OnInit {
  i = 4;
  editCache = {};
  dataSet = [
    {
      key: '0',
      name: 'Edward King',
      age: '33',
      nationality: 'British',
    },
    {
      key: '1',
      name: 'Bella Rossi',
      age: '29',
      nationality: 'Italian',
    },
    {
      key: '2',
      name: 'Hana Song (송하나)',
      age: '19',
      nationality: 'Korean',
    },
    {
      key: '3',
      name: 'Veronica Garcia',
      age: '23',
      nationality: 'Argentine',
    },
    {
      key: '4',
      name: 'Fatima Ali',
      age: '45',
      nationality: 'Lebanese',
    },
  ];

  constructor() {}

  ngOnInit(): void {
    this.updateEditCache();
  }

  addRow(): void {
    this.i++;
    this.dataSet = [
      ...this.dataSet,
      {
        key: `${this.i}`,
        name: `New User - ${this.i}`,
        age: '32',
        nationality: ``,
      },
    ];
    this.updateEditCache();
  }

  deleteRow(i: string): void {
    const dataSet = this.dataSet.filter((d) => d.key !== i);
    this.dataSet = dataSet;
  }

  startEdit(key: string): void {
    this.editCache[key].edit = true;
  }

  finishEdit(key: string): void {
    this.editCache[key].edit = false;
    this.dataSet.find((item) => item.key === key).name = this.editCache[
      key
    ].name;
  }

  updateEditCache(): void {
    this.dataSet.forEach((item) => {
      if (!this.editCache[item.key]) {
        this.editCache[item.key] = {
          edit: false,
          name: item.name,
        };
      }
    });
  }
}
