require('dotenv').config();
const { IamAuthenticator } = require('ibm-watson/auth');
const AssistantV2 = require('ibm-watson/assistant/v2');

let authenticator;
if (process.env.ASSISTANT_IAM_APIKEY) {
  authenticator = new IamAuthenticator({
    apikey: process.env.ASSISTANT_IAM_APIKEY,
  });
}
var assistant = new AssistantV2({
  version: '2020-04-01',
  authenticator,
  serviceName: 'assistant',
  url: process.env.ASSISTANT_IAM_URL,
  disableSslVerification:
    process.env.DISABLE_SSL_VERIFICATION === 'true' ? true : false,
});

module.exports = assistant;
