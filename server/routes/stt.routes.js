const router = require('express').Router();
const { STTController } = require('../controllers');
const logger = require('../config/logger');

const sttController = new STTController();

router.post('/getToken', (req, res) => {
  logger.info('[Speech to Text - /getToken - POST]');
  sttController.getToken(req, res);
});

module.exports = router;
