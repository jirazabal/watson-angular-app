import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};

@Injectable({
  providedIn: 'root',
})
export class TtsService {
  host = environment.url;
  path = '/api/tts/getToken';

  url = this.host + this.path;

  constructor(private http: HttpClient) {}

  fetchTTSToken(): Observable<any> {
    return this.http.post(this.url, null, httpOptions).pipe(
      map((res) => res),
      catchError(this.handleError)
    );
  }
  private handleError(res: HttpErrorResponse) {
    console.error(res.error);
    return Observable.throw(res.error || 'Server error');
  }
}
