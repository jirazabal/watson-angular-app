const { assistant } = require('../config');
const logger = require('../config/logger');

class AssistantController {
  postMessage(body, res) {
    const payload = {
      assistantId: process.env.ASSISTANT_ID,
      sessionId: body.sessionId,
      input: {
        text: String(body.message),
      },
      context: body.context,
    };

    assistant
      .message(payload)
      .then((response) => {
        logger.info('Watson Assistant - message request success');
        res.status(200).send(response.result);
      })
      .catch((err) => {
        console.log('error: ', err);
        res.status(500).send(err);
      });
  }

  getSessionId(req, res) {
    assistant
      .createSession({
        assistantId: process.env.ASSISTANT_ID || '{assistant_id}',
      })
      .then((response) => {
        logger.info('Watson Assistant - getSessionId request success');
        res.status(200).send(response.result);
      })
      .catch((err) => {
        console.log('error: ', err);
        res.status(500).send(err);
      });
  }
}

module.exports = AssistantController;
