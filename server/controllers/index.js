module.exports = {
  AssistantController: require('./assistant.controller'),
  STTController: require('./stt.controller'),
  TTSController: require('./tts.controller'),
};
