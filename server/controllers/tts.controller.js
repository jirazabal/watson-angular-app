const { IamTokenManager } = require('ibm-watson/auth');
const logger = require('../config/logger');

class STTController {
  getToken(req, res) {
    const ttsAuthenticator = new IamTokenManager({
      apikey: process.env.TEXT_TO_SPEECH_IAM_APIKEY,
    });

    return ttsAuthenticator
      .requestToken()
      .then(({ result }) => {
        logger.info('Watson Text to Speech: Auth request success');
        res.json({
          accessToken: result.access_token,
          url: process.env.TEXT_TO_SPEECH_URL,
        });
      })
      .catch((e) => logger.error(e));
  }
}

module.exports = STTController;
